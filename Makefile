all:
	dot -Tps -Gsize=7.5,7.5 qt5.dot -o qt5.ps
	ps2pdf qt5.ps
	dot -Tps -Gsize=7.5,7.5 kde-frameworks.dot -o kde-frameworks.ps
	ps2pdf kde-frameworks.ps
	dot -Tps -Gsize=7.5,7.5 plasma.dot -o plasma.ps
	ps2pdf plasma.ps
	dot -Tps -Gsize=7.5,7.5 kde-applications.dot -o kde-applications.ps
	dot -Tps kde-frameworks-and-plasma.dot -o kde-frameworks-and-plasma.ps
	dot -Tps kde-all.dot -o kde-all.ps
